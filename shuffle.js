function shuffleUsers(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1)); // retourne un index entre 0 et i
        [array[i], array[j]] = [array[j], array[i]]; //inverse les indices
    }
    return array;
}

let testarray = ['james', 'lily', 'martin', 'sarah'];

//mélange le tableau original
// let result = shuffleUsers(testarray);
// alert(result);

//crée un second tableau mélangé
let result2 = shuffleUsers(testarray.slice());

alert("tableau original : " + testarray + ", nouveau tableau : " + result2);



function divideArray(allGroupArray, groupSize){
    let grouparray = [];
    for (i = 0; i < allGroupArray.length; i = i + groupSize) {
        myGroup = allGroupArray.slice(i, i + groupSize);
        grouparray.push(myGroup);
    }
    return grouparray;
}

//Diviser en groupes de deux cad en TABLEAUX de 2
let groupsoftwo = ['mimi', 'momo', 'gaga', 'gigi'];
//alert(groupsoftwo);
let groupsresult = divideArray(groupsoftwo, 2);
//let groupsresult = divideArray(['mimi', 'momo', 'gaga', 'gigi'], 2);
console.log(groupsresult);
