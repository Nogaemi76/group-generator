const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const routes = require('./routes/api');

const app = express();

mongoose.connect('mongodb://localhost/userbase', { useNewUrlParser: true });
mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use('/api', routes);
app.use((err, res, req, next) => {
    //console.log(err);
    res.status(422).send({error: err.message});
    //error 422 Unprocessable Entity
});

//process.env.port permet de changer le port par ex 4444
//avec la commande PORT=4444 node index.js
app.listen(process.env.port || 4000, () => {
    console.log('listening for requests on port 4000');
    
});