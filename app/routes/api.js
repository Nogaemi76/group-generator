const express = require('express');
const router = express.Router();
const User = require('../models/user')

//get a list of names from the db
router.get('/names', (req, res, next) => {
    User.find({}).then((users) => {
        res.send(users);
    })
});

//add a new name to the db
router.post('/names', (req, res, next) => {

    //Create an instance of user and save it to mongodb
    User.create(req.body).then((user) => {
        res.send(user);
    }).catch(next);
});


//delete a name in the db
router.delete('/names/:id', (req, res, next) => {
    User.findByIdAndRemove({_id:req.params.id}).then((user) => {
        res.send(user);
    });
});

module.exports = router;